"""

Дана абстрактная файловая система вида:
├─ dir1
│ └─ dir2
│ ├─ file1
│ └─ file2
└─ dir3
В каждой директории содержится либо файлы, либо поддиректория (вложенность не ограничена),
либо она может быть пустой.
Требуется написать функцию biggestPath(X: dict) -> str, принимающую в качестве аргумента
словарь X (описывающий файловую систему) и возвращающую самый длинный найденный в нем
корректный путь в виде строки (либо строку "/" если такой путь в словаре не найден).
Критерии корректного пути:
- ключ словаря (подсловаря) интерпретировать как имя директории
- значение словаря (подсловаря) интерпретировать как набор поддиректорий (вложенный словарь)
или файлов (вложенный список)
- имена директорий/файлов должны состоят только из букв английского алфавита и цифр
- длина пути должна быть не более 255 символов (с учетом знака / , разделяющего компоненты пути)
- в одной папке не могут находится директории/файлы с одинаковыми именами

## Пример 1.

python
[IN]
d1 = {'dir1': {},
    'dir2': ['file1'],
    'dir3':
        {'dir4': ['file2'],
        'dir5':
            {'dir6':
                {'dir7': {}}}}}

[OUT]
/dir3/dir5/dir6/dir7

## Пример 2.

python
[IN]
d2 = {'dir1': ['file1', 'file1']}

[OUT]
/

## Пример 3.

python
[IN]
d3 = {'dir1': ['file1', 'file2', 'file2']}

[OUT]
/dir1/file1

"""

d1 = {'dir1': {}, 'dir2': ['file1'], 'dir3': {'dir4': ['file2'], 'dir5': {'dir6': {'dir7': {}}}}}
d2 = {'dir1': ['file1', 'file1']}
d3 = {'dir1': ['file1', 'file2', 'file2']}


def eng_num(x):
    res = True
    if len(x) <= 254:
        for i in range(len(x)):
            if 'a' <= x[i] <= "z" or 'A' <= x[i] <= 'Z' or '0' <= x[i] <= '9':
                pass
            else:
                res = False
    else:
        res = False
    return res


def paths(some_dict, path=()):
    for key, value in some_dict.items():
        if eng_num(key):
            key_path = path + (key, )
            if isinstance(value, list):
                res = (list(set(x for x in value if value.count(x) < 2)))
                for i in range(len(res)):
                    if eng_num(res[i]) is False:
                        del res[i]
                if res:
                    key_path += (res[0],)
                else:
                    key_path = path

            yield key_path
            if hasattr(value, 'items'):
                yield from paths(value, key_path)


def biggestPath(x_dict):
    bigstr = []
    bigpath = "/"
    for i in paths(x_dict):
        bigstr.append(i)
    listp = list(max(bigstr))
    return bigpath+"/".join(listp)


def main():
    print("d1")
    print(biggestPath(d1))
    print("d2")
    print(biggestPath(d2))
    print("d3")
    print(biggestPath(d3))


if __name__ == "__main__":
    main()

